import Vue from 'vue'
import VueRouter from 'vue-router'
import Layout from '@/views/layouts/Layout.vue'
import Welcome from '@/views/Welcome.vue'

Vue.use(VueRouter)

const routes = [
  // {
  //   path: '/',
  //   name: 'home',
  //   component: HomeView
  // },
  // {
  //   path: '/about',
  //   name: 'about',
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () => import(/* webpackChunkName: 'about' */ '../views/AboutView.vue')
  // }
  {
    path: '/',
    name: 'Welcome',
    component: Welcome,
    meta: {
      requiresAuth: false
    }
  },
  {
    path: '/about-us',
    redirect: '/about-us',
    component: Layout,
    meta: {
      requiresAuth: false
    },
    children: [
      {
        path: '/about-us',
        name: 'about-us',
        component: () =>
          import(
            /* webpackChunkName: 'about-us' */ '@/views/pages/AboutUs.vue'
          )
      },
      {
        path: '/contact-us',
        name: 'contact-us',
        component: () =>
          import(
            /* webpackChunkName: 'contact-us' */ '@/views/pages/ContactUs.vue'
          )
      },
      {
        path: '/our-product',
        name: 'our-product',
        component: () =>
          import(
            /* webpackChunkName: 'our-product' */ '@/views/pages/OurProduct.vue'
          )
      }
    ]
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
